package com.rabbitmq.utils;

import com.rabbitmq.client.*;
import com.rabbitmq.work.WorkConsumer;

import java.io.IOException;
import java.util.Scanner;
import java.util.concurrent.TimeoutException;

/**
 * @Author: zshui
 * @Description:
 * @Date: Create in 2021/5/11 22:04
 * @Modified By :
 */
public class GlobalConsumer {

    public static void createConsumer(String queue,Integer num){

        for (int i = 0; i < num; i++) {
            new Thread(new GlobalConsumer.WorkConsumerClazz()).start();
        }

    }


    public static class WorkConsumerClazz implements Runnable{

        @Override
        public void run() {
            //1、连接工厂
            ConnectionFactory connectionFactory = new ConnectionFactory();
            // 2: 设置连接属性
            connectionFactory.setHost("192.168.109.129");
            connectionFactory.setPort(5672);
            connectionFactory.setVirtualHost("/");
            connectionFactory.setUsername("root");
            connectionFactory.setPassword("1234");

            Connection connection = null;
            Channel channel = null;
            try {
                connection = connectionFactory.newConnection();
                channel = connection.createChannel();
                //每次消费条记录
                channel.basicQos(1);
                // 需要设置手动应答
                channel.basicConsume("workQueue", false, new DefaultConsumer(channel) {
                    @Override
                    public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                        System.out.println(Thread.currentThread().getName() + " 消费了 ： " + new String(body, "UTF-8"));
                        this.getChannel().basicAck(envelope.getDeliveryTag(), false);
                    }
                });
                new Scanner(System.in).next();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                // 关闭通道
                if (channel != null) {
                    try {
                        channel.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (TimeoutException e) {
                        e.printStackTrace();
                    }
                }

                //关闭连接
                if (connection != null) {
                    try {
                        connection.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}
