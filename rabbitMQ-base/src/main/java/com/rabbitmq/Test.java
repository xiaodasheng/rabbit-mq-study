package com.rabbitmq;

import com.rabbitmq.client.*;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;
import java.util.concurrent.TimeoutException;

/**
 * @Author: zshui
 * @Description:
 * @Date: Create in 2021/5/18 19:45
 * @Modified By :
 */
public class Test {

    public static void main(String[] args) {

        //1、连接工厂
        ConnectionFactory connectionFactory = new ConnectionFactory();
        // 2: 设置连接属性
        connectionFactory.setHost("192.168.109.129");
        connectionFactory.setPort(5672);
        connectionFactory.setVirtualHost("/");
        connectionFactory.setUsername("root");
        connectionFactory.setPassword("1234");

        Connection connection = null;
        Channel channel = null;
        try {
            connection = connectionFactory.newConnection("哈哈");
            channel = connection.createChannel();

//            channel.exchangeDeclare("test_exchange", "fanout", false, true, null);
            channel.exchangeDeclare("test_exchange", "direct", false, true, null);

            channel.queueDeclare("testQueue1", false, true, true,  null);
            channel.queueDeclare("testQueue2", false, true, true,  null);

            channel.queueBind("testQueue1", "test_exchange", "ss");
            channel.queueBind("testQueue2", "test_exchange", "ss");


            for (int i = 0; i < 10; i++) {
                String msg = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")) + " : 测试一下  " + i;
                System.out.println("第" + (i+1) + "次发送成功");
                channel.basicPublish("test_exchange", "ss", null, msg.getBytes());
            }

            Scanner scanner = new Scanner(System.in);
            scanner.next();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // 关闭通道
            if (channel != null) {
                try {
                    channel.close();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (TimeoutException e) {
                    e.printStackTrace();
                }
            }
            //关闭连接
            if (connection != null) {
                try {
                    connection.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }
}
