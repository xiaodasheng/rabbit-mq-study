package com.rabbitmq.fanout.simple;

import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.Scanner;
import java.util.concurrent.TimeoutException;

/**
 * @Author: zshui
 * @Description:
 * @Date: Create in 2021/5/6 21:10
 * @Modified By :
 */
public class SimpleConsumer {

    public static void main(String[] args) {
        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setHost("192.168.109.129");
        connectionFactory.setPort(5672);
        connectionFactory.setVirtualHost("/");
        connectionFactory.setUsername("root");
        connectionFactory.setPassword("1234");

        Connection connection = null;
        Channel channel = null;
        try{
            //获取连接
            connection = connectionFactory.newConnection("这里可以取个连接的名字");
            //获取通道
            channel = connection.createChannel();
            //接收消息
            /**
             * queue:队列名
             * deliverCallback： 当一个消息发送过来后的回调接口
             * cancelCallback：当一个消费者取消订阅时的回调接口;取消消费者订阅队列时除了使用{@link Channel#basicCancel}之外的所有方式都会调用该回调方法
             *
             * autoAck：true 接收到传递过来的消息后acknowledged（应答服务器），false 接收到消息后不应答服务器
             * consumerTag:客户端生成的一个消费者标识，用来区分多个消费者
             * nolocal:如果服务器不应将在此通道连接上发布的消息传递给此使用者，则为true;请注意RabbitMQ服务器上不支持此标记
             * exclusive: 如果是单个消费者，则为true
             * arguments:消费的一组参数
             * deliverCallback： 当一个消息发送过来后的回调接口
             * shutdownSignalCallback: 当channel/connection 关闭后回调
            */
           channel.basicConsume("testQueue", false,new DefaultConsumer(channel){
               @Override
               public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                   System.out.println("handleDelivery方法中的:"+consumerTag);
                   System.out.println(new String(body, "UTF-8"));
               }
           });


            Scanner scanner = new Scanner(System.in);
            String next = scanner.next();

        } catch (TimeoutException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if (channel != null) {
                try {
                    channel.close();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (TimeoutException e) {
                    e.printStackTrace();
                }
            }

            if (connection != null) {
                try {
                    connection.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

}
