package com.rabbitmq.ttl;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeoutException;

/**
 * @Author: zshui
 * @Description:
 * @Date: Create in 2021/5/11 22:03
 * @Modified By :
 */
public class TtlProducer {

    public static void main(String[] args) {
        ConnectionFactory connectionFactory = new ConnectionFactory();
        // 2: 设置连接属性
        connectionFactory.setHost("192.168.109.129");
        connectionFactory.setPort(5672);
        connectionFactory.setVirtualHost("/");
        connectionFactory.setUsername("root");
        connectionFactory.setPassword("1234");


        Connection connection = null;
        Channel channel = null;
        try {
            connection = connectionFactory.newConnection();
            channel = connection.createChannel();
            //声明交换机
            channel.exchangeDeclare("ttl_exchange", "direct", false, false, false, null);

            //声明超时队列
            Map<String, Object> map = new HashMap<>(16);
            map.put("x-message-ttl", 100000);
            channel.queueDeclare("ttl_queue", true, false, false, map);
            //绑定关系
            channel.queueBind("ttl_queue", "ttl_exchange", "");

//            channel.basicPublish("ttl_exchange", "", null, "数据哦".getBytes("UTF-8"));

            Map<String, Object> headers = new HashMap<>(16);
            headers.put("aaa", "nnn");
            //发送会超时的消息
            AMQP.BasicProperties properties = new AMQP.BasicProperties().builder()
                    // 传送方式
                    .deliveryMode(1)
                    // 编码方式
                    .contentEncoding("UTF-8")
                    // 过期时间
                    .expiration("2000")
                    //自定义属性
                    .headers(headers).build();
            channel.basicPublish("ttl_exchange", "", properties, "数据哦".getBytes("UTF-8"));

            System.out.println("发完了");
        } catch (Exception e) {
            e.printStackTrace();
        }finally {

            if (channel != null) {
                try {
                    channel.close();
                } catch (TimeoutException | IOException e) {
                    e.printStackTrace();
                }
            }

            if (connection != null) {
                try {
                    connection.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }


    }

}
