package com.rabbitmq.ttl;

import com.rabbitmq.utils.GlobalConsumer;

/**
 * @Author: zshui
 * @Description:
 * @Date: Create in 2021/5/11 22:21
 * @Modified By :
 */
public class TtlConsumer {

    public static void main(String[] args) {
        GlobalConsumer.createConsumer("ttl_queue", 2);
    }
}
