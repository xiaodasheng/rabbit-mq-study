package com.rabbitmq.work;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.TimeoutException;

/**
 * @Author: zshui
 * @Description:
 * @Date: Create in 2021/5/7 20:59
 * @Modified By :
 */
public class WorkProducer {

    public static void main(String[] args) {
        //1、连接工厂
        ConnectionFactory connectionFactory = new ConnectionFactory();
        // 2: 设置连接属性
        connectionFactory.setHost("192.168.109.129");
        connectionFactory.setPort(5672);
        connectionFactory.setVirtualHost("/");
        connectionFactory.setUsername("root");
        connectionFactory.setPassword("1234");

        Connection connection = null;
        Channel channel = null;
        try {
            connection = connectionFactory.newConnection("word生产者");
            channel = connection.createChannel();

            channel.queueDeclare("workQueue", false, false, false,  null);

            String time = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
            String msg = time + " : 使用word模式";

            for (int i = 0; i < 10; i++) {
                channel.basicPublish("", "workQueue", false, null, msg.getBytes());
                System.out.println("第" + (i+1) + "次发送成功");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            // 关闭通道
            if (channel != null) {
                try {
                    channel.close();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (TimeoutException e) {
                    e.printStackTrace();
                }
            }
            //关闭连接
            if (connection != null) {
                try {
                    connection.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
