package com.rabbitmq.work;

import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.Scanner;
import java.util.concurrent.TimeoutException;

/**
 * @Author: zshui
 * @Description:
 * @Date: Create in 2021/5/7 21:00
 * @Modified By :
 */
public class WorkConsumer {

    public static void main(String[] args) {

        new Thread(new WorkConsumerClazz("小兵")).start();
        new Thread(new WorkConsumerClazz("将军")).start();
    }


    public static class WorkConsumerClazz implements Runnable{
        private String name;
        public WorkConsumerClazz(String name) {
            this.name = name;
        }

        @Override
        public void run() {
            //1、连接工厂
            ConnectionFactory connectionFactory = new ConnectionFactory();
            // 2: 设置连接属性
            connectionFactory.setHost("192.168.109.129");
            connectionFactory.setPort(5672);
            connectionFactory.setVirtualHost("/");
            connectionFactory.setUsername("root");
            connectionFactory.setPassword("1234");

            Connection connection = null;
            Channel channel = null;
            try {
                connection = connectionFactory.newConnection(name);
                channel = connection.createChannel();
                // 声明队列
//                channel.queueDeclare("workQueue", false, false, false,  null);
                //每次消费条记录
                channel.basicQos(1);
                // 需要设置手动应答
                channel.basicConsume("workQueue", false, new DefaultConsumer(channel) {
                    @Override
                    public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                        System.out.println("【" + name + "】 消费了 ： " + new String(body, "UTF-8"));
                        if ( "小兵".equals(name)) {
                            try {
                                Thread.sleep(2000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                        this.getChannel().basicAck(envelope.getDeliveryTag(), false);
                    }
                });


                new Scanner(System.in).next();

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                // 关闭通道
                if (channel != null) {
                    try {
                        channel.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (TimeoutException e) {
                        e.printStackTrace();
                    }
                }
                //关闭连接
                if (connection != null) {
                    try {
                        connection.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}
