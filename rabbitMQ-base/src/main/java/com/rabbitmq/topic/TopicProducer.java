package com.rabbitmq.topic;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.Scanner;
import java.util.concurrent.TimeoutException;

/**
 * @Author: zshui
 * @Description:
 * @Date: Create in 2021/5/7 22:17
 * @Modified By :
 */
public class TopicProducer {

    public static void main(String[] args) {
        //1、连接工厂
        ConnectionFactory connectionFactory = new ConnectionFactory();
        // 2: 设置连接属性
        connectionFactory.setHost("192.168.109.129");
        connectionFactory.setPort(5672);
        connectionFactory.setVirtualHost("/");
        connectionFactory.setUsername("root");
        connectionFactory.setPassword("1234");

        Connection connection = null;
        Channel channel = null;
        try {
            connection = connectionFactory.newConnection("topic");
            channel = connection.createChannel();

//            channel.queueDelete("topic_queue_sms");
//            channel.queueDelete("topic_queue_vx");
//            channel.queueDelete("topic_queue_email");

            //声明
            channel.exchangeDeclare("topic_exchange", "topic", false, true, false, null);
            //声明通道
            channel.queueDeclare("topic_queue_sms", false, true, true, null);
            channel.queueDeclare("topic_queue_vx", false, true, true, null);
            channel.queueDeclare("topic_queue_email", false, true, true, null);
            //绑定   *代表至少一层层级  # 代表0到正无穷和层级
            channel.queueBind("topic_queue_sms", "topic_exchange", "sms.*");
            channel.queueBind("topic_queue_vx", "topic_exchange", "vx.#");
            channel.queueBind("topic_queue_email", "topic_exchange", "#.email.#");

            String msg = "我这是topic类型的 数据";
            channel.basicPublish("topic_exchange", "sms.email", null, msg.getBytes());

            System.out.println("发完了");
            new Scanner(System.in).next();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // 关闭通道
            if (channel != null) {
                try {
                    channel.close();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (TimeoutException e) {
                    e.printStackTrace();
                }
            }
            //关闭连接
            if (connection != null) {
                try {
                    connection.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
