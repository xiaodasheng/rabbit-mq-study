package com.rabbitmq.direct;

import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.Scanner;
import java.util.concurrent.TimeoutException;

/**
 * @Author: zshui
 * @Description:
 * @Date: Create in 2021/5/7 21:50
 * @Modified By :
 */
public class DirectProducer {

    public static void main(String[] args) {

        //1、连接工厂
        ConnectionFactory connectionFactory = new ConnectionFactory();
        // 2: 设置连接属性
        connectionFactory.setHost("192.168.109.129");
        connectionFactory.setPort(5672);
        connectionFactory.setVirtualHost("/");
        connectionFactory.setUsername("root");
        connectionFactory.setPassword("1234");

        Connection connection = null;
        Channel channel = null;
        try {
            connection = connectionFactory.newConnection("direct");
            channel = connection.createChannel();
            //声明交换机
            channel.exchangeDeclare("direct_exchange", "direct", false, false, false, null);
            //声明通道
            channel.queueDeclare("directQueue", false, false, false, null);
            //声明通道
            channel.queueDeclare("directQueue2", false, false, false, null);
            //交换机绑定通道
            channel.queueBind("directQueue", "direct_exchange", "routKey");
            channel.queueBind("directQueue2", "direct_exchange", "routKey2");

            String msg = "嘿嘿，直发送到directQueue中的数据";
            for (int i = 0; i < 20; i++) {
                channel.basicPublish("direct_exchange", "routKey", null, msg.getBytes());
            }
            System.out.println("发完了");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // 关闭通道
            if (channel != null) {
                try {
                    channel.close();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (TimeoutException e) {
                    e.printStackTrace();
                }
            }
            //关闭连接
            if (connection != null) {
                try {
                    connection.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
