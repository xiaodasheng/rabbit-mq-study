package com.rabbitmq.direct;

import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.Scanner;
import java.util.concurrent.TimeoutException;

/**
 * @Author: zshui
 * @Description:
 * @Date: Create in 2021/5/7 21:58
 * @Modified By :
 */
public class DirectConsumer {

    public static void main(String[] args) {
        //1、连接工厂
        ConnectionFactory connectionFactory = new ConnectionFactory();
        // 2: 设置连接属性
        connectionFactory.setHost("192.168.109.129");
        connectionFactory.setPort(5672);
        connectionFactory.setVirtualHost("/");
        connectionFactory.setUsername("root");
        connectionFactory.setPassword("1234");

        Connection connection = null;
        Channel channel = null;
        try {
            connection = connectionFactory.newConnection("direct");
            channel = connection.createChannel();

            channel.basicConsume("directQueue",true,new DefaultConsumer(channel){
                @Override
                public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                    System.out.println("directQueue 接受到信息咯:" + new String(body, "UTF-8"));
                }
            });

            channel.basicConsume("directQueue2",true,new DefaultConsumer(channel){
                @Override
                public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                    System.out.println("directQueue2 接受到信息咯: "+new String(body, "UTF-8"));
                }
            });

            new Scanner(System.in).next();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // 关闭通道
            if (channel != null) {
                try {
                    channel.close();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (TimeoutException e) {
                    e.printStackTrace();
                }
            }
            //关闭连接
            if (connection != null) {
                try {
                    connection.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
