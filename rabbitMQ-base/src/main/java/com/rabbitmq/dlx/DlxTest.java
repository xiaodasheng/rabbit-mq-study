package com.rabbitmq.dlx;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeoutException;

/**
 * @Author: zshui
 * @Description:    用于创建死信队列的例子
 * @Date: Create in 2021/5/12 21:23
 * @Modified By :
 */
public class DlxTest {

    public static void main(String[] args) {
        ConnectionFactory connectionFactory = new ConnectionFactory();
        // 2: 设置连接属性
        connectionFactory.setHost("192.168.109.129");
        connectionFactory.setPort(5672);
        connectionFactory.setVirtualHost("/");
        connectionFactory.setUsername("root");
        connectionFactory.setPassword("1234");


        Connection connection = null;
        Channel channel = null;
        try {
            connection = connectionFactory.newConnection();
            channel = connection.createChannel();

            //创建 死信交换机
            channel.exchangeDeclare("d_exchange_dlx", "fanout", true, true, null);
            //创建死信队列
            channel.queueDeclare("d_queue_dlx", true, true, true, null);
            channel.queueBind("d_queue_dlx", "d_exchange_dlx", "key");


            //创建交换机
            channel.exchangeDeclare("d_exchange", "direct", true, true,  null);
            Map<String, Object> map = new HashMap<>(4);
            map.put("x-dead-letter-exchange", "d_exchange_dlx");
            map.put("x-dead-letter-routing-key", "key");
            //创建交换机绑定的队列
            channel.queueDeclare("d_queue", true, true, true, map);
            //队列绑定
            channel.queueBind("d_queue", "d_exchange", "");


            //发送过期信息
            AMQP.BasicProperties.Builder builder = new AMQP.BasicProperties().builder();
            builder.expiration("6000");
            channel.basicPublish("d_exchange", "", builder.build(), "啊哈哈哈哈".getBytes());

            System.out.println("发完了");
        } catch (Exception e) {
            e.printStackTrace();
        }finally {

            if (channel != null) {
                try {
                    channel.close();
                } catch (TimeoutException | IOException e) {
                    e.printStackTrace();
                }
            }

            if (connection != null) {
                try {
                    connection.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
    }
}
