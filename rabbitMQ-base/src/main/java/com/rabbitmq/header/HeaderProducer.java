package com.rabbitmq.header;

import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.concurrent.TimeoutException;

/**
 * @Author: zshui
 * @Description:
 * @Date: Create in 2021/5/10 22:19
 * @Modified By :
 */
public class HeaderProducer {

    public static void main(String[] args) {
        ConnectionFactory configurator = new ConnectionFactory();
        configurator.setHost("192.168.109.129");
        configurator.setPort(5672);
        configurator.setVirtualHost("/");
        configurator.setUsername("root");
        configurator.setPassword("1234");

        Connection connection = null;
        Channel channel = null;
        try {
            // 连接
            connection = configurator.newConnection("Header连接");
            // 创建
            channel = connection.createChannel();
            //声明交换机
            channel.exchangeDeclare("header_exchange", "headers", false, false, null);
            //声明队列
            channel.queueDeclare("header_queue", false, false, false, null);

            Map<String, Object> headers = new Hashtable<String, Object>();
            headers.put("aaa", "01234");
            headers.put("bbb", "56789");

            //绑定
            channel.queueBind("header_queue", "header_exchange", "", headers);

            //声明队列
            channel.queueDeclare("header_queue2", false, false, false, null);
            Map<String, Object> headers2 = new Hashtable<String, Object>();
            headers2.put("bbb", "56789");
            headers2.put("ccc", "98765");
            channel.queueBind("header_queue2", "header_exchange", "", headers2);

            //声明队列
            channel.queueDeclare("header_queue3", false, false, false, null);
            Map<String, Object> headers3 = new Hashtable<String, Object>();
            headers3.put("ccc", "98765");
            headers3.put("ddd", "43210");
            channel.queueBind("header_queue3", "header_exchange", "", headers3);

            Map<String, Object> headers1 = new HashMap<>(4);
            headers1.put("ddd", "43210");
            headers1.put("ccc", "98765");
            AMQP.BasicProperties.Builder builder = new AMQP.BasicProperties().builder();
            builder.headers(headers1);

            channel.basicPublish("header_exchange", "", builder.build(), "header 你们好".getBytes());
            System.out.println("发完了");
        } catch (IOException | TimeoutException e) {
            e.printStackTrace();
        }finally {
            if (channel != null) {
                try {
                    channel.close();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (TimeoutException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }


    }
}
