﻿# 1、交换机参数解析：

```
channel.exchangeDeclare(String exchange, String type, boolean durable, boolean autoDelete, boolean internal, Map<String, Object> arguments)
```

 1. exchange: 交换器的名称
 2. type: 交换器的类型, 常见类型有fanout, direct, topic, headers
 3. durable: 设置是否持久化, true表示持久化, 反之是非持久化, 持久化可以将交换器存盘, 在服务器重启的时候不会丢失相关信息
 4. autoDelete: 设置是否自动删除, true表示自动删除, 自动删除的前提是至少有一个队列或者交换器与这个交换器绑定, 之后所有与这个交换器绑定的队列或交换器都于此解绑, 注意不能错误的把这个参数理解为"当与此交换器连接的客户端都断开时, RabbitMQ会自动删除本交换器"
 5. internal: 设置是否内置的, true表示是内置的交换器, 客户端程序无法直接发送消息到这个交换器中, 只能通过交换器路由到交换器这个方式
 6. arguments: 其他一些结构化参数, 比如alternate-exchange


# 2、队列参数解析：
```
channel.queueDeclare(String queue, boolean durable, boolean exclusive, boolean autoDelete, Map<String, Object> arguments)
```

 1. queue: 队列名称
 2. durable： 是否持久化, 队列的声明默认是存放到内存中的，如果rabbitmq重启会丢失，如果想重启之后还存在就要使队列持久化，保存到Erlang自带的Mnesia数据库中，当rabbitmq重启之后会读取该数据库
 3. exclusive：是否排外的，有两个作用，一：当连接关闭时connection.close()该队列是否会自动删除；二：该队列是否是私有的private，如果不是排外的，可以使用两个消费者都访问同一个队列，没有任何问题，如果是排外的，会对当前队列加锁，其他通道channel是不能访问的，如果强制访问会报异常：com.rabbitmq.client.ShutdownSignalException: channel error; protocol method: #method<channel.close>(reply-code=405, reply-text=RESOURCE_LOCKED - cannot obtain exclusive access to locked queue 'queue_name' in vhost '/', class-id=50, method-id=20)一般等于true的话用于一个队列只能有一个消费者来消费的场景
 4. autoDelete：是否自动删除，当最后一个消费者断开连接之后队列是否自动被删除，可以通过RabbitMQ Management，查看某个队列的消费者数量，当consumers = 0时队列就会自动删除
 5. arguments：其他参数
    - Message TTL(x-message-ttl)：设置队列中的所有消息的生存周期(统一为整个队列的所有消息设置生命周期), 也可以在发布消息的时候单独为某个消息指定剩余生存时间,单位毫秒, 类似于redis中的ttl，生存时间到了，消息会被从队里中删除，注意是消息被删除，而不是队列被删除， 特性Features=TTL, 单独为某条消息设置过期时间AMQP.BasicProperties.Builder properties = new AMQP.BasicProperties().builder().expiration(“6000”);

    - Auto Expire(x-expires): 当队列在指定的时间没有被访问(consume, basicGet, queueDeclare…)就会被删除,Features=Exp

    - Max Length(x-max-length): 限定队列的消息的最大值长度，超过指定长度将会把最早的几条删除掉， 类似于mongodb中的固定集合，例如保存最新的100条消息, Feature=Lim

    - Max Length Bytes(x-max-length-bytes): 限定队列最大占用的空间大小， 一般受限于内存、磁盘的大小, Features=Lim B

    - Dead letter exchange(x-dead-letter-exchange)： 当队列消息长度大于最大长度、或者过期的等，将从队列中删除的消息推送到指定的交换机中去而不是丢弃掉,Features=DLX

    - Dead letter routing key(x-dead-letter-routing-key)：将删除的消息推送到指定交换机的指定路由键的队列中去, Feature=DLK

    - Maximum priority(x-max-priority)：优先级队列，声明队列时先定义最大优先级值(定义最大值一般不要太大)，在发布消息的时候指定该消息的优先级， 优先级更高（数值更大的）的消息先被消费,

    - Lazy mode(x-queue-mode=lazy)： Lazy Queues: 先将消息保存到磁盘上，不放在内存中，当消费者开始消费的时候才加载到内存中

    - Master locator(x-queue-master-locator): ?

# 3、绑定消息参数解析：
```
channel.queueBind(String queue, String exchange, String routingKey, Map<String, Object> arguments)
```

 1. queue: 队列名称
 2. exchange: 交换器的名称
 3. routingKey: 交换机绑定队列的routingKey （在fanout和headers模式下不生效）
 4. arguments：交换机绑定队列的参数

# 4、发送消息参数解析：


```
channel.basicPublish(String exchange, String routingKey, BasicProperties props, byte[] body);
```

1. exchange: 交换机名称
2. routingKey: 路由key(fanout和topic模式下使用)
3. props: 参数
    - String contentType, //消息内容的类型
    - String contentEncoding, //消息内容的编码格式
	- Map<String,Object> headers,//header类型的交换机可以用到
    - Integer deliveryMode,//消息持久化 1 不持久化 2 持久化
    - Integer priority,//优先级
    - String correlationId, //关联id
    - String replyTo,//通常用于命名回调队列
    - String expiration,//设置过期消息过期时间
    - String messageId, //消息id
    - Date timestamp, //消息的时间戳
    - String type,  //类型
    - String userId, //用户ID
    - String appId, //应用程序id
    - String clusterId //集群id
4. body: 发送的信息





body: 发送的信息
