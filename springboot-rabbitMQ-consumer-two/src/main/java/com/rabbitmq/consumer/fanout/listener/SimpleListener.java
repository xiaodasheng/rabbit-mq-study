package com.rabbitmq.consumer.fanout.listener;

import com.alibaba.fastjson.JSON;
import com.rabbitmq.client.Channel;
import com.rabbitmq.consumer.bean.UserProducerDTO;
import com.rabbitmq.consumer.fanout.constant.SimpleConstant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * @Author: zshui
 * @Description:
 * @Date: Create in 2021/5/22 16:32
 * @Modified By :
 */
@Slf4j
@Component
@RabbitListener(queues = {SimpleConstant.SIMPLE_QUEUE})
public class SimpleListener {

    @RabbitHandler
    public void consumer(String userDTO, Channel channel, Message message) throws IOException {
        try {
            log.info("{}接收到的数据：{}", Thread.currentThread().getName(), userDTO);
            UserProducerDTO userProducerDTO = JSON.parseObject(userDTO, UserProducerDTO.class);
            if (userProducerDTO.getSex() > 1) {
                throw new RuntimeException("错误");
            }
            // 确认收到消息，false只确认当前consumer一个消息收到，true确认所有consumer获得的消息
            channel.basicAck(message.getMessageProperties().getDeliveryTag(),false);
        } catch (Exception e) {
            if (message.getMessageProperties().getRedelivered()) {
                log.info("{}消息已重复处理失败,拒绝再次接收 : {}", Thread.currentThread().getName(), userDTO);
                // 拒绝消息，requeue=false 表示不再重新入队，如果配置了死信队列则进入死信队列。
                channel.basicReject(message.getMessageProperties().getDeliveryTag(), false);
            }else{
                log.info("{}消息已重复处理失败,拒绝再次接收 : {}",Thread.currentThread().getName(), userDTO);
                //  确认否定消息,第一个boolean表示一个consumer还是所有，第二个boolean表示requeue是否重新回到队列，true重新入队
                channel.basicNack(message.getMessageProperties().getDeliveryTag(), false, true);
            }
        }
    }
}
