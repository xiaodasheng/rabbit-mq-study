package com.rabbitmq.consumer.handler;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @Author: zshui
 * @Description:    全局异常捕获
 * @Date: Create in 2021/5/22 16:09
 * @Modified By :
 */
@Slf4j
@RestControllerAdvice
public class ExceptionController {

    /**
     * @Author: zshui
     * @Description : 全局捕获异常
     * @Date: 2021/5/22 16:15
     * @param e
     * @return java.lang.String
     **/
    @ExceptionHandler
    public String exception(Exception e) {
        return "出错了，错误是：" + e.getMessage();
    }
}
