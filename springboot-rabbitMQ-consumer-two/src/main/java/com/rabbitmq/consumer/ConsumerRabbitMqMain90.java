package com.rabbitmq.consumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Author: zshui
 * @Description:
 * @Date: Create in 2021/5/29 12:41
 * @Modified By :
 */
@SpringBootApplication
public class ConsumerRabbitMqMain90 {

    public static void main(String[] args) {
        SpringApplication.run(ConsumerRabbitMqMain90.class, args);
    }
}

