package com.rabbitmq.consumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Author: zshui
 * @Description:
 * @Date: Create in 2021/5/26 22:44
 * @Modified By :
 */
@SpringBootApplication
public class ProducerRabbitMqMain {

    public static void main(String[] args) {
        SpringApplication.run(ProducerRabbitMqMain.class, args);
    }
}
