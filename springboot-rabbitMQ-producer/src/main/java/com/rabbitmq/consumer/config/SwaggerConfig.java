package com.rabbitmq.consumer.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: zshui
 * @Description:
 * @Date: Create in 2021/5/24 23:01
 * @Modified By :
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket docket(){
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("rabbitMQ")
                .apiInfo(this.apiInfo())
                .globalOperationParameters(this.operationParameters())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.rabbitmq"))
                .paths(PathSelectors.any())
                .build();
    }

    private List<Parameter> operationParameters(){
        List<Parameter> list = new ArrayList<>();
        ParameterBuilder builder = new ParameterBuilder();

        list.add(builder.name("param")
                .description("参数")
                .defaultValue("xxx")
                .modelRef(new ModelRef("string"))
                .parameterType("query").build());

        return list;
    }

    private ApiInfo apiInfo(){
        return new ApiInfoBuilder()
                .title("又是标题")
                .description("又是描述")
                .version("又是版本: v-1.0")
                .termsOfServiceUrl("https://www.billbill.com")
                .license("许可证")
                .licenseUrl("https://www.baidu.com")
                .contact(new Contact("小占","https://www.gitee.com","1154742732@qq.com"))
                .build();
    }

}
