package com.rabbitmq.consumer.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

/**
 * @Author: zshui
 * @Description:    只确认消息是否正确到达 Exchange 中
 *              1. 如果消息没有到exchange,则confirm回调,ack=false
 *              2. 如果消息到达exchange,则confirm回调,ack=true
 * @Date: Create in 2021/5/26 22:14
 * @Modified By :
 */
@Slf4j
public class RabbitMqConfirmCallback implements RabbitTemplate.ConfirmCallback {

    @Override
    public void confirm(CorrelationData correlationData, boolean ack, String cause) {
        log.info("消息发送: 【{}】 , 数据：【{}】", ack, ack ? correlationData : cause);
    }
}
