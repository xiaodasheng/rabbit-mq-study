package com.rabbitmq.consumer.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.utils.SerializationUtils;

/**
 * @Author: zshui
 * @Description:    消息没有正确到达队列时触发回调，如果正确到达队列不执行
 *                  exchange到queue成功,则不回调return
 *                  exchange到queue失败,则回调return
 * @Date: Create in 2021/5/26 22:18
 * @Modified By :
 */
@Slf4j
public class RabbitMqReturnCallback implements  RabbitTemplate.ReturnCallback {

    @Override
    public void returnedMessage(Message message, int replyCode, String replyText, String exchange, String routingKey) {
        // 反序列化对象输出
        log.info("消息主体: {}", SerializationUtils.deserialize(message.getBody()));
        log.info("应答码: {}",replyCode );
        log.info("描述: {}", replyText);
        log.info("消息使用的交换器 exchange : {}",exchange);
        log.info("消息使用的路由键 routing : {}", routingKey);
    }
}
