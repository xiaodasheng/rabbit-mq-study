package com.rabbitmq.consumer.fanout.constant;

/**
 * @Author: zshui
 * @Description:
 * @Date: Create in 2021/5/22 8:30
 * @Modified By :
 */
public class SimpleConstant {

    /**
     * 队列的名称
     */
    public static final String SIMPLE_QUEUE_1 = "simple_queue_1";
    public static final String SIMPLE_QUEUE_2 = "simple_queue_2";

    /**
     *  交换机名称
     */
    public static final String SIMPLE_FANOUT = "simple_fanout";


    /**
     * 死信交换机名称
     */
    public static final String DEAD_EXCHANGE = "ddl_exchange";

    /**
     * 死信队列名称
     */
    public static final String DEAD_QUEUE = "dead_queue";

    /**
     *  路由key
     */
    public static final String DEAD_QUEUE_ROUTE_KEY = "dead_route";

}
