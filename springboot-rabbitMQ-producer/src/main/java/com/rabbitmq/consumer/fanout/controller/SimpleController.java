package com.rabbitmq.consumer.fanout.controller;

import com.alibaba.fastjson.JSON;
import com.rabbitmq.consumer.fanout.constant.SimpleConstant;
import com.rabbitmq.consumer.fanout.dto.SendDto;
import com.rabbitmq.consumer.fanout.po.SendPo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: zshui
 * @Description:
 * @Date: Create in 2021/5/22 8:24
 * @Modified By :
 */
@Slf4j
@Api(tags = "Fanout类型下的简单模式")
@RestController
@RequestMapping("/fanout/simple")
public class SimpleController {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @PostMapping("/send")
    @ApiOperation(value = "根据参数来发送交换机数据")
    public String test(@RequestBody SendPo sendPo) {
        log.info("接收到的队列：{} ", sendPo);
        log.debug("debug了：{} ", sendPo);
        SendDto build = SendDto.builder().build();

        BeanUtils.copyProperties(sendPo,build);
        // routingKey如果是 fanout也可以用
        rabbitTemplate.convertAndSend(SimpleConstant.SIMPLE_FANOUT, null, JSON.toJSONString(build));
        return "发送好了";
    }

}
