package com.rabbitmq.consumer.fanout.config;

import com.rabbitmq.consumer.fanout.constant.SimpleConstant;
import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author: zshui
 * @Description:
 * @Date: Create in 2021/5/20 23:07
 * @Modified By :
 */

@Configuration
public class SimpleConfig {

    /**
     * @Author: zshui
     * @Description : 声明队列,不绑定交换机(绑定默认交换机),懒加载模式(没有对队列操作  则不创建)
     * @Date: 2021/5/22 16:11
     * @param
     * @return Queue
     **/
    @Bean
    public Queue simpleQueue1(){
        return new Queue(SimpleConstant.SIMPLE_QUEUE_1, false, false, false, this.getArguments());
    }
    @Bean
    public Queue simpleQueue2(){
        return new Queue(SimpleConstant.SIMPLE_QUEUE_2, false, false, false, this.getArguments());
    }

    @Bean
    public FanoutExchange simpleFanout(){
        return new FanoutExchange(SimpleConstant.SIMPLE_FANOUT, false, false);
    }

    @Bean
    public Binding simpleBinding(FanoutExchange simpleFanout, Queue simpleQueue1) {
        return BindingBuilder.bind(simpleQueue1).to(simpleFanout);
    }

    @Bean
    public Binding simpleBinding2(FanoutExchange simpleFanout, Queue simpleQueue2) {
        return BindingBuilder.bind(simpleQueue2).to(simpleFanout);
    }

    /**
     * @Author: zshui
     * @Description : 获取队列参数
     * @Date: 2021/5/29 11:16
     * @param
     * @return java.util.Map<java.lang.String,java.lang.Object>
     **/
    private Map<String,Object> getArguments(){
        Map<String, Object> arguments = new HashMap<>(10);
        arguments.put("x-dead-letter-exchange", SimpleConstant.DEAD_EXCHANGE);
        arguments.put("x-dead-letter-routing-key", SimpleConstant.DEAD_QUEUE_ROUTE_KEY);
//        arguments.put("x-message-ttl", 10000);
        return arguments;
    }

    /**
     * @Author: zshui
     * @Description : 配置死信队列
     * @Date: 2021/5/29 10:47
     * @param
     * @return org.springframework.amqp.core.Queue
     **/
    @Bean
    public Queue deadQueue(){
        return new Queue(SimpleConstant.DEAD_QUEUE, true, false, false);
    }

    /**
     * @Author: zshui
     * @Description : 配置死信队列的交换机
     * @Date: 2021/5/29 10:45
     * @param
     * @return org.springframework.amqp.core.Exchange
     **/
    @Bean
    public DirectExchange ddlExchange(){
        return new DirectExchange(SimpleConstant.DEAD_EXCHANGE, true, false);
    }

    /**
     * @param deadQueue
     * @param ddlExchange
     * @return org.springframework.amqp.core.Binding
     * @Author: zshui
     * @Description : 死信队列绑定
     * @Date: 2021/5/29 10:50
     **/
    @Bean
    public Binding ddlBing(Queue deadQueue, DirectExchange ddlExchange) {
        return BindingBuilder.bind(deadQueue).to(ddlExchange).with(SimpleConstant.DEAD_QUEUE_ROUTE_KEY);
    }

}
