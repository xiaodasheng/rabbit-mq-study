package com.rabbitmq.consumer.fanout.po;

import com.rabbitmq.consumer.fanout.constant.SimpleConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * @Author: zshui
 * @Description:
 * @Date: Create in 2021/5/27 22:44
 * @Modified By :
 */
@Data
@Builder
@ApiModel("发送交换机的信息")
@NoArgsConstructor
@AllArgsConstructor
public class SendPo {

    @ApiModelProperty(value = "名字", example = "小占", required = true)
    private String name;

    @ApiModelProperty(value = "年龄", example = "1", required = true)
    private Integer sex;

    @ApiModelProperty(value = "日期", example = "2020-12-18 00:00:00", required = true)
    private LocalDateTime birthday;

    @ApiModelProperty(value = "队列名", example = SimpleConstant.SIMPLE_QUEUE_1, required = true)
    private String queueName;
}
