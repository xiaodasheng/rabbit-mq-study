package com.rabbitmq.consumer.fanout.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * @Author: zshui
 * @Description:
 * @Date: Create in 2021/5/27 22:46
 * @Modified By :
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SendDto {

    private String name;

    private Integer sex;

    private LocalDateTime birthday;


}
