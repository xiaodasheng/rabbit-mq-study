package com.rabbitmq.consumer.bean;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * @Author: zshui
 * @Description:
 * @Date: Create in 2021/5/22 16:27
 * @Modified By :
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserProducerDTO {

    private String name;

    private Integer sex;

    private LocalDateTime birthday;

}
