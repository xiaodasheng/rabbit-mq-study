package com.rabbitmq.consumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Author: zshui
 * @Description:
 * @Date: Create in 2021/5/29 12:37
 * @Modified By :
 */
@SpringBootApplication
public class ConsumerRabbitMqMain91 {

    public static void main(String[] args) {
        SpringApplication.run(ConsumerRabbitMqMain91.class, args);
    }
}

