package com.rabbitmq.consumer.fanout.constant;

/**
 * @Author: zshui
 * @Description:
 * @Date: Create in 2021/5/22 8:30
 * @Modified By :
 */
public class SimpleConstant {

    /**
     * 队列的名称
     */
    public static final String SIMPLE_QUEUE = "simple_queue_1";

}
